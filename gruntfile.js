'use strict';
module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            all: [
                'Gruntfile.js',
                'assets/js'
            ],
            options: {
                jshintrc: '.jshintrc'
            }
        },
        connect: {
            server: {
                options: {
                    port: 9001,
                    base: 'app',
                    keepalive: true
                }
            }
        },
        protractor: {
            cucumber: {
                options : {
                    configFile: 'e2e-tests/config/protractor.cucumber.conf.js',
                    keepAlive: true
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-protractor-runner');


    grunt.registerTask(
        'serve', ['jshint','connect']
    );

    grunt.registerTask(
        'e2e', ['protractor']
    );
};