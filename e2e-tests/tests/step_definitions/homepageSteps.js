'use strict';
var world = require('../../config/world.js'),
    pages = require('../../config/pageCollection.js');

var homepage = function() {

    this.Then(/^I search text "([^"]*)"$/, function (searchText, callback) {
        pages.homepage.setSearchText(searchText)
            .then (function(){
            callback();
        });
    });
    
    this.Then(/^I should see a product "([^"]*)"$/, function (product, callback) {
        world.expect(pages.homepage.getProduct().getText()).to.eventually.equal(product)
            .then (function(){
            callback();
        });
    });

    this.Then(/^I add product to the cart$/, function (callback) {
        pages.homepage.addToCart()
            .then (function(){
                callback();
            });
    });

    this.Then(/^I go to to cart page$/, function (callback) {
        pages.homepage.goToCart()
            .then (function(){
                callback();
            });
    });
};

module.exports = homepage;