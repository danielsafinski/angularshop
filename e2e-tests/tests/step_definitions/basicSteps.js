'use strict';
var basicSteps = function() {

    this.Given(/^I am on the "([^"]*)"$/, function(partialHtml, callback) {
        browser.get(partialHtml).then (function(){
            callback();
        });
    });

};

module.exports = basicSteps;
