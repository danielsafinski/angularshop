'use strict';
var world = require('../../config/world.js'),
    pages = require('../../config/pageCollection.js');

var homepage = function() {

    this.Then(/^I search text "([^"]*)"$/, function (searchText, callback) {
        pages.homepage.setSearchText(searchText)
            .then (function(){
                callback();
            });
    });

    this.Then(/I checking if product exist on cart$/, function (callback) {
        var product = pages.cart.cartTable();
        product.then(function (table) {
            world.expect(table[0]).to.equal('Flovent HFA');
        })
            .then (function(){
            callback();
        });
            });


};

module.exports = homepage;