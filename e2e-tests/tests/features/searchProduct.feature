Feature: Search product

  Scenario: Search product on homepage
    Given I am on the "#"
    When I search text "Warfarin Sodium"
    Then I should see a product "Warfarin Sodium"