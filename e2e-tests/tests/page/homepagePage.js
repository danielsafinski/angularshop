'use strict';
var Page = require('astrolabe').Page,
    world = require('../../config/world.js');

var self = Page.create({
    url: {value: ''},

    navBarHeader: {
        get: function () {
            return element(self.by.css('.navbar-header'));
        }
    },

    search: {
        get: function () {
            return element(self.by.css('input[ng-model="search"]'));
        }
    },
    
    btnCart:{
        get: function () {
            return element(self.by.css('button[ng-click="addToCart( product )"]'));
        }
    },

    cartPage:{
        get: function () {
            return element(self.by.linkText('Koszyk'));
        }
    },



    getProduct: {
        value: function () {
            return element(self.by.binding('product.name'));
        }
    },
    setSearchText: {
        value: function (searchText) {
            return browser.wait(world.EC.presenceOf(self.search), 3000)
                .then(function () {
                    self.search.sendKeys(searchText);
                });
        }
    },
    goToCart: {
        value: function () {
            return browser.wait(world.EC.presenceOf(self.cartPage), 3000)
                .then(function () {
                    self.cartPage.click();
                });
        }
    },
    
    addToCart: {
        value: function () {
            return browser.wait(world.EC.presenceOf(self.btnCart), 3000)
                .then(function () {
                    self.btnCart.click();
                });
        }
    }

});
module.exports = self;