'use strict';

var Page = require('astrolabe').Page,
    world = require('../../config/world.js');

var self = Page.create({
    url: {value: '/cart'},

    cartTable: {
        value: function () {
            var zn = element.all(by.repeater('item in cart').column('item.name')).map(function (elm) {
                return elm.getText();
            });
            return zn;
        }
    },
    
    items: {
        value: function () {
            return browser.wait(world.EC.presenceOf(self.cartTable), 3000)
                .then(function () {
                    self.cartTable.map(function (elm) {
                        return {
                            text: elm.getText()
                        };
                    });
                })
        }
    }

});
module.exports = self;