
exports.config = {
  framework: 'cucumber',
  seleniumArgs: [],
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: 'http://angularshop.azurewebsites.net/',

  onPrepare: function () {
    browser.driver.manage().window().maximize();
  },
  
  verbose: true,
  
  specs: ['../tests/features/*.feature'],
  chromeOnly: false,
  

  multiCapabilities: [
    {
      'browserName': 'chrome',
      'chromeOptions': {
        'args': ['--test-type', 'show-fps-counter=true']
      }
    }
  ],
  cucumberOpts: {
    // format: '',
    require: ['../tests/step_definitions/*.js',
      '../tests/step_definitions/**/*.js']
  },

  allScriptsTimeout: 30000,


  rootElement: 'body'
};
