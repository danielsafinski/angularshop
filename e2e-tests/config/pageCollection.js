var pageCollection = (function () {
    
    var  homepage = require('../tests/page/homepagePage'),
         cart = require('../tests/page/cartPage');

    return {
        homepage: homepage,
        cart: cart
    };

}());
module.exports = pageCollection;

