# NSE - uat_test (Protractor+Cucumber)

***

# Requirements

* [NodeJS](http://nodejs.org/)
* [Selenium-standalone](https://www.npmjs.com/package/selenium-standalone)

# Instalation and running test

**1.  Install NodeJS and NPM**

    Firstly we have to install nodejs
  
    Ubuntu
    http://richardhsu.net/2013/10/19/installing-nodejs-npm-on-ubuntu-13-10/
    
    Mac
    http://coolestguidesontheplanet.com/installing-node-js-on-osx-10-10-yosemite/
    
    Windows
    http://nodejs.org/
    
**2.  Install Selenium-Standalone** 

   npm install selenium-standalone@latest -g
   selenium-standalone install
    
    
    If you cannot run grunt on windows or another system, please install this package with global flag:
        
          npm install -g grunt
          npm install -g grunt-cli

    If you want run features on Internet Explorer:
        
        1. Open IE
        2. Go to Tools -> Internet Options -> Security
        3. Set all zones to the same protected mode, disabled.
       *[Set Zone](https://selenium.googlecode.com/issues/attachment?aid=17950019000&name=1.png&token=ABZ6GAdA_fRtPRqnYvFZKrbRY7EE_jg-JQ%3A1453383774929&id=1795&mod_ts_token=ABZ6GAc4wyVB0NR5TWVqxfT2Db7CXsBPEg%3A1453383774929&inline=1)



3. Run tests
    
    Basically to launch tests please use "grunt e2e"
* ###### Chrome
* ###### Mozzilla Firefox
* ###### Internet Explorer
